package com.itamecodes.testassignment_br.loaders;

import android.content.Context;
import android.util.Log;

import com.itamecodes.testassignment_br.models.StoreList;
import com.itamecodes.testassignment_br.networking.StoreRestAdapter;
import com.itamecodes.testassignment_br.networking.StoreRestService;

import java.io.IOException;

import retrofit.RetrofitError;


public class StoreDataLoader extends WrappedAsyncTaskLoader<StoreList> {

    public StoreDataLoader(Context context) {
        super(context);
    }

    @Override
    public StoreList loadInBackground() {
        StoreRestService service = null;
        try {
            service = StoreRestAdapter.getRestService();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StoreList storeList = null;
        try {
            storeList = service.getStoreList();
        } catch (RetrofitError e) {

        }
        return storeList;
    }
}
