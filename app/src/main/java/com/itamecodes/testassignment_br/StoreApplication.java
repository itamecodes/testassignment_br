package com.itamecodes.testassignment_br;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;


public class StoreApplication extends Application {

    private static Context mApplicationContext;
    private static StoreApplication mApplication;
    public StoreApplication() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationContext=this.getApplicationContext();
        mApplication=this;
    }

    public static StoreApplication getApplication(){
        return mApplication;
    }

    public static Context getAppContext(){
        return mApplicationContext;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static boolean isMarshMallow() {
        return (Build.VERSION.SDK_INT>= Build.VERSION_CODES.M);
    }
}
