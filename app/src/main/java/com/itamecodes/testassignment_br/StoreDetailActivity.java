package com.itamecodes.testassignment_br;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.itamecodes.testassignment_br.models.Store;
import com.itamecodes.testassignment_br.utils.Constants;


public class StoreDetailActivity extends AppCompatActivity {

    private Store mStore;
    private int mPositionInListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        if (savedInstanceState == null) {
            mStore = getIntent().getParcelableExtra(StoreDetailFragment.STORE_ITEM);
            mPositionInListView = getIntent().getIntExtra(Constants.ITEM_POS, -1);
            StoreDetailFragment fragment = StoreDetailFragment.getInstance(mStore);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.store_detail_container, fragment)
                    .commit();
        } else {
            mStore = savedInstanceState.getParcelable(StoreDetailFragment.STORE_ITEM);
            mPositionInListView = savedInstanceState.getInt(Constants.ITEM_POS,-1);
        }
        if (actionBar != null) {
            actionBar.setTitle(mStore.getName());
        }
        if(getResources().getBoolean(R.bool.isDualPane)){
            Intent intent=new Intent(this,StoreListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(Constants.IS_DUAL_PANE, true);
            intent.putExtra(StoreDetailFragment.STORE_ITEM,mStore);
            intent.putExtra(Constants.ITEM_POS,mPositionInListView);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent h = NavUtils.getParentActivityIntent(this);
            h.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            NavUtils.navigateUpTo(this, h);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(StoreDetailFragment.STORE_ITEM, mStore);
        savedInstanceState.putInt(Constants.ITEM_POS, mPositionInListView);
        super.onSaveInstanceState(savedInstanceState);
    }

}
