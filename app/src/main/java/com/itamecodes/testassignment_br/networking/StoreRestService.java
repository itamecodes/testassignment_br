package com.itamecodes.testassignment_br.networking;


import com.itamecodes.testassignment_br.models.StoreList;

import retrofit.http.GET;

public interface StoreRestService {

    @GET("/BR_Android_CodingExam_2015_Server/stores.json")
    StoreList getStoreList();
}
