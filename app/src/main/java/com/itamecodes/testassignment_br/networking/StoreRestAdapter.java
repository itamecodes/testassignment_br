package com.itamecodes.testassignment_br.networking;



import com.itamecodes.testassignment_br.StoreApplication;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.io.IOException;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class StoreRestAdapter {
    public static final String URL="http://sandbox.bottlerocketapps.com/";

    private static StoreRestService mService = null;

    public synchronized static StoreRestService getRestService() throws IOException {
        if(mService==null) {
            File httpCacheDirectory=new File(StoreApplication.getAppContext().getCacheDir(),"responses");
            Cache cache = null;
            cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);
            OkHttpClient okHttpClient = new OkHttpClient();
            if (cache != null) {
                okHttpClient.setCache(cache);
            }
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(URL)
                    .setClient(new OkClient(okHttpClient))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("Accept", "application/json;versions=1");
                            if (StoreApplication.getApplication().isNetworkConnected()) {
                                int maxAge = 60; // read from cache for 1 minute
                                request.addHeader("Cache-Control", "public, max-age=" + maxAge);
                            } else {
                                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                                request.addHeader("Cache-Control",
                                        "public, only-if-cached, max-stale=" + maxStale);
                            }
                        }
                    })
                    .build();

            restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
            mService = restAdapter.create(StoreRestService.class);
        }
        return mService;
    }
}
