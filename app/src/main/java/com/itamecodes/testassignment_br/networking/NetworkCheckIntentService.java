package com.itamecodes.testassignment_br.networking;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.itamecodes.testassignment_br.utils.Constants;


/**
 * Created by vivek on 6/5/15.
 */
public class NetworkCheckIntentService extends IntentService {

    public NetworkCheckIntentService() {
        super("NetworkCheckIntentService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);
        return START_STICKY;

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent intentConnection = new Intent(Constants.NO_NETWORK_ACTION);
        final ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            intentConnection.putExtra(Constants.CONNECTION_EXISTS, true);
        } else {
            intentConnection.putExtra(Constants.CONNECTION_EXISTS, false);
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(intentConnection);

    }
}