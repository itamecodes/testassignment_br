package com.itamecodes.testassignment_br.networking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.itamecodes.testassignment_br.utils.Constants;


/**
 * Created by vivek on 6/4/15.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("vivekconn","1234");
        Intent msgIntent = new Intent(context, NetworkCheckIntentService.class);
        msgIntent.putExtra(Constants.CONNECTION_EXISTS, "");
        context.startService(msgIntent);
    }

}
