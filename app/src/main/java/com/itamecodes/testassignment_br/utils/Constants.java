package com.itamecodes.testassignment_br.utils;


public class Constants {
    public static final String URL="http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server/stores.json";
    public static final String CONNECTION_EXISTS ="isconnected" ;
    public static final String NO_NETWORK_ACTION ="no_network_action" ;
    public static final String IS_FIRST_LAUNCH ="isfirstlaunch" ;
    public static final String IS_DUAL_PANE = "isdualpane";
    public static final String ITEM_POS ="itempos" ;
}
