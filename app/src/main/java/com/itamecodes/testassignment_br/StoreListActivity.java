package com.itamecodes.testassignment_br;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itamecodes.testassignment_br.loaders.StoreDataLoader;
import com.itamecodes.testassignment_br.models.Store;
import com.itamecodes.testassignment_br.models.StoreList;
import com.itamecodes.testassignment_br.utils.Constants;
import com.itamecodes.testassignment_br.utils.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of Stores. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link StoreDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class StoreListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<StoreList> {

    private static final int LOADER_ID = 1;
    private static final int REQUEST_LOCATION = 2;
    private static final String APP_PREFS = "appprefs";
    private static SimpleItemRecyclerViewAdapter mAdapter;
    private boolean mIsList = true;
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private RecyclerView mRecyclerView;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list);

        //set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        // have just put up a dummy menu item:- settings which does not do anything
        toolbar.showOverflowMenu();

      //the shared preferences to track if it is first launch
        mSharedPreferences = getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        //ok demoing marshmallow's runtime permissions
        if (StoreApplication.isMarshMallow()) {
            //we need your location permission to show your location on the map.. and we request this permission only the first time
            if (mSharedPreferences.getBoolean(Constants.IS_FIRST_LAUNCH, true)) {
                checkLocationPermissionsOnMarshmallow();
            }
        }
        //ok first launch is done .. and we have checked for marshmallow permissions already
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(Constants.IS_FIRST_LAUNCH, false);
        editor.commit();


        mRecyclerView = (RecyclerView) findViewById(R.id.store_list);
        setupRecyclerView(mRecyclerView);

        //device orientation w-900dp is going to be treated as two pane
        if (findViewById(R.id.store_detail_container) != null) {
            mTwoPane = true;
        }
        //start the loader and load the data
        getSupportLoaderManager().initLoader(LOADER_ID, null, this);

        if (getSupportFragmentManager().findFragmentById(R.id.store_detail_container) != null) {
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.store_detail_container)).commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mAdapter = new SimpleItemRecyclerViewAdapter(new ArrayList<Store>());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new SpacesItemDecoration(10));
    }

    @Override
    public Loader<StoreList> onCreateLoader(int loaderId, Bundle args) {
        if (loaderId == LOADER_ID) {
            return new StoreDataLoader(getApplicationContext());
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<StoreList> loader, StoreList data) {
        if(data!=null) {
            mAdapter.addData(data);
        }else{
           if(!((StoreApplication)getApplication()).isNetworkConnected()){
               toggleNetworkDisplay(false);
           }else{
               Snackbar.make(mRecyclerView, getString(R.string.error_occurred), Snackbar.LENGTH_LONG).show();
           }
        }

    }

    @Override
    public void onLoaderReset(Loader<StoreList> loader) {

    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Store> mStoreList;
        private int selectedItem=-1;


        public SimpleItemRecyclerViewAdapter(List<Store> storeList) {
            mStoreList = storeList;
        }


        public void addData(StoreList storeList) {
            mStoreList.addAll(storeList.getStores());
            notifyDataSetChanged();

        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.store_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mStoreList.get(position);
            String url = holder.mItem.getStoreLogoURL();
            Glide.with(StoreListActivity.this).load(url).dontTransform().dontAnimate().into(holder.mLogo);
            holder.mPhoneNumber.setText(mStoreList.get(position).getPhone());
            holder.mAddress.setText(mStoreList.get(position).getAddress());
            //a lazy way to show the item selected.. ideally this should be a selector
            if(position==selectedItem){
                holder.mView.setBackgroundColor(ContextCompat.getColor(StoreListActivity.this,R.color.bg_gray_light));
            }else{
                holder.mView.setBackgroundColor(ContextCompat.getColor(StoreListActivity.this,R.color.white));
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   selectedItem=position;
                    if (mTwoPane) {
                        StoreDetailFragment fragment = StoreDetailFragment.getInstance(holder.mItem);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.store_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, StoreDetailActivity.class);
                        intent.putExtra(StoreDetailFragment.STORE_ITEM, holder.mItem);
                        intent.putExtra(Constants.ITEM_POS,position);
                        context.startActivity(intent);
                    }
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mStoreList.size();
        }

        public void setSelectedItem(int selectedItem) {
            this.selectedItem = selectedItem;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView mLogo;
            public final TextView mPhoneNumber;
            public final TextView mAddress;
            public Store mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mLogo = (ImageView) view.findViewById(R.id.logo);
                mPhoneNumber = (TextView) view.findViewById(R.id.phonenumber);
                mAddress = (TextView) view.findViewById(R.id.address);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mItem.getName() + "'";
            }
        }
    }


    private static Snackbar snackBar;
    private BroadcastReceiver mNoNetworkReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("vivekconn","123");
            //depending on if the network is available or not , we show a snackbar of indefinite length informing user of no network
            boolean connectionReceived = intent.getBooleanExtra(Constants.CONNECTION_EXISTS, false);
            toggleNetworkDisplay(connectionReceived);
        }
    };


    private void toggleNetworkDisplay(boolean isNetworkAvailable) {
        if (isNetworkAvailable) {
            if (snackBar != null) {
                if (snackBar.isShown()) {
                    snackBar.dismiss();
                    getSupportLoaderManager().restartLoader(LOADER_ID, null, this);
                }
            }
        } else {
            if (snackBar == null) {
                snackBar = Snackbar.make(mRecyclerView, getString(R.string.no_network_available), Snackbar.LENGTH_INDEFINITE);
            } else {
                snackBar.show();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mNoNetworkReceiver, new IntentFilter(Constants.NO_NETWORK_ACTION));
        toggleNetworkDisplay(StoreApplication.getApplication().isNetworkConnected());

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNoNetworkReceiver);


    }


    public boolean checkLocationPermissionsOnMarshmallow() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            } else {
                new AlertDialog.Builder(StoreListActivity.this)
                        .setTitle(getString(R.string.permission_location_title))
                        .setMessage(R.string.permission_location_text)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                StoreListActivity.this.startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", StoreListActivity.this.getPackageName(), null)));
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
            return false;
        } else {
            return true;
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to , but we dont need any location permission here.. only place is while clicking the fab on storedetail page...
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public void onNewIntent(Intent newIntent) {
        super.onNewIntent(newIntent);
        boolean isDualPane = newIntent.getBooleanExtra(Constants.IS_DUAL_PANE, false);
        if (isDualPane) {
            Store store = newIntent.getParcelableExtra(StoreDetailFragment.STORE_ITEM);
            int position = newIntent.getIntExtra(Constants.ITEM_POS, -1);
            if (position != -1) {
                mAdapter.setSelectedItem(position);
                mAdapter.notifyDataSetChanged();
            }
            if (store != null) {
                StoreDetailFragment fragment = StoreDetailFragment.getInstance(store);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.store_detail_container, fragment)
                        .commit();
            }
        }
    }

}


