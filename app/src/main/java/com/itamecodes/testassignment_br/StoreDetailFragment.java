package com.itamecodes.testassignment_br;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.itamecodes.testassignment_br.models.Store;

/**
 * A fragment representing a single Store detail screen.
 * This fragment is either contained in a {@link StoreListActivity}
 * in two-pane mode (on tablets) or a {@link StoreDetailActivity}
 * on handsets.
 */
public class StoreDetailFragment extends Fragment implements OnMapReadyCallback {

    public static final String STORE_ITEM = "store_item";
    private static final int REQUEST_CALL = 4;
    private TextView mStorePhone;
    private ImageView mStoreLogo;

    private Store mStore;
    private ShareActionProvider mShareActionProvider;
    private FloatingActionButton mCallFloatingButton;


    public StoreDetailFragment() {
    }

    public static StoreDetailFragment getInstance(Store store) {
        StoreDetailFragment frag = new StoreDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(STORE_ITEM, store);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(STORE_ITEM)) {
            mStore = getArguments().getParcelable(STORE_ITEM);
            Activity activity = this.getActivity();
            Toolbar appBarLayout = (Toolbar) activity.findViewById(R.id.detail_toolbar);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mStore.getName());
            } else {
                appBarLayout = (Toolbar) activity.findViewById(R.id.toolbar);
                if (appBarLayout != null) {
                    appBarLayout.setTitle(mStore.getName());
                }
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.store_detail, container, false);
        mCallFloatingButton = (FloatingActionButton) rootView.findViewById(R.id.fab_storedetail);
        mStorePhone = ((TextView) rootView.findViewById(R.id.detail_store_phone));
        mStoreLogo = ((ImageView) rootView.findViewById(R.id.storedetail_logo));
        if (mStore != null) {
            ((TextView) rootView.findViewById(R.id.store_detail)).setText(mStore.getName());
            ((TextView) rootView.findViewById(R.id.detail_store_address)).setText(mStore.getAddress());
            ((TextView) rootView.findViewById(R.id.detail_store_city)).setText(mStore.getCity());
            ((TextView) rootView.findViewById(R.id.detail_store_state)).setText(mStore.getState());
            ((TextView) rootView.findViewById(R.id.detail_store_zipcode)).setText(mStore.getZipcode());
            mStorePhone.setText(mStore.getPhone());
            Glide.with(StoreDetailFragment.this).load(mStore.getStoreLogoURL()).dontTransform().dontAnimate().into(mStoreLogo);
            mStorePhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callStore();
                }
            });

        }
        setHasOptionsMenu(true);
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.main_content);
        // The View with the BottomSheetBehavior
        final View bottomSheet = coordinatorLayout.findViewById(R.id.bottomsheet_storedetail);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                } else {
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        behavior.setPeekHeight(180);

        mCallFloatingButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                callStore();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapfragment);
        mapFragment.getMapAsync(this);
        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        LatLng storeLatLong = new LatLng(Double.valueOf(mStore.getLatitude()), Double.valueOf(mStore.getLongitude()));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(storeLatLong, 13));
        map.addMarker(new MarkerOptions()
                .title(mStore.getName())
                .snippet(mStore.getAddress())
                .position(storeLatLong));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.share_menu, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        mShareActionProvider.setShareIntent(createShareIntent());
        super.onCreateOptionsMenu(menu, inflater);
    }

    private Intent createShareIntent() {
        String storeAddress = mStore.getAddress();
        String storeName = mStore.getName();
        StringBuilder textToShare_sb = new StringBuilder();
        textToShare_sb.append(String.format(getString(R.string.share_text), storeName, storeAddress));
        String textToShare = textToShare_sb.toString();
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
        shareIntent.setType("text/plain");
        return shareIntent;
    }

    public void callStore() {
        if (mStore.getPhone() != null && !mStore.getPhone().isEmpty()) {
            if (checkForCallPermission()) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + mStore.getPhone()));
                startActivity(intent);
            }
        }
    }

    public boolean checkForCallPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_CALL);
            } else {
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.permission_title_phone_calls))
                        .setMessage(getString(R.string.permission_text_phone_calls))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getActivity().getPackageName(), null)));
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }

        }
        return false;

    }
}
