package com.itamecodes.testassignment_br.models;


import android.os.Parcel;
import android.os.Parcelable;

public class Store implements Parcelable {

    private String address;
    private String city;
    private String name;
    private String latitude;
    private String zipcode;
    private String storeLogoURL;
    private String phone;
    private String longitude;
    private String storeID;
    private String state;

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     *
     * @param zipcode
     * The zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     *
     * @return
     * The storeLogoURL
     */
    public String getStoreLogoURL() {
        return storeLogoURL;
    }

    /**
     *
     * @param storeLogoURL
     * The storeLogoURL
     */
    public void setStoreLogoURL(String storeLogoURL) {
        this.storeLogoURL = storeLogoURL;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The storeID
     */
    public String getStoreID() {
        return storeID;
    }

    /**
     *
     * @param storeID
     * The storeID
     */
    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeString(this.city);
        dest.writeString(this.name);
        dest.writeString(this.latitude);
        dest.writeString(this.zipcode);
        dest.writeString(this.storeLogoURL);
        dest.writeString(this.phone);
        dest.writeString(this.longitude);
        dest.writeString(this.storeID);
        dest.writeString(this.state);
    }

    public Store() {
    }

    private Store(Parcel in) {
        this.address = in.readString();
        this.city = in.readString();
        this.name = in.readString();
        this.latitude = in.readString();
        this.zipcode = in.readString();
        this.storeLogoURL = in.readString();
        this.phone = in.readString();
        this.longitude = in.readString();
        this.storeID = in.readString();
        this.state = in.readString();
    }

    public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {
        public Store createFromParcel(Parcel source) {
            return new Store(source);
        }

        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}


