package com.itamecodes.testassignment_br.models;



import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class StoreList implements Parcelable {

    private List<Store> stores = new ArrayList<Store>();

    /**
     *
     * @return
     * The stores
     */
    public List<Store> getStores() {
        return stores;
    }

    /**
     *
     * @param stores
     * The stores
     */
    public void setStores(List<Store> stores) {
        this.stores = stores;
    }



    protected StoreList(Parcel in) {
        if (in.readByte() == 0x01) {
            stores = new ArrayList<Store>();
            in.readList(stores, Store.class.getClassLoader());
        } else {
            stores = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (stores == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(stores);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StoreList> CREATOR = new Parcelable.Creator<StoreList>() {
        @Override
        public StoreList createFromParcel(Parcel in) {
            return new StoreList(in);
        }

        @Override
        public StoreList[] newArray(int size) {
            return new StoreList[size];
        }
    };
}